# Cofee Finder

List of dependencies:

- [connected-react-router](https://github.com/supasate/connected-react-router)
- [formik](https://jaredpalmer.com/formik/)
- [material-ui](https://material-ui.com/)
- [react-redux](https://react-redux.js.org/)
- [react-router-dom](https://reacttraining.com/react-router/web/guides/quick-start)
- [redux-act](https://github.com/pauldijou/redux-act)
- [redux-persist](https://github.com/rt2zz/redux-persist)
- [redux-saga](https://redux-saga.js.org/)
- [lodash](https://lodash.com/)
- [yup](https://github.com/jquense/yup)

### Getting Started

Just clone the repo and start hacking:

```bash
$ yarn                             # Installs dependencies
$ yarn start                       # Compile the app and opens it in a browser with "live reload"
```

Then open [http://localhost:3000/](http://localhost:3000/) to see your app.<br>
