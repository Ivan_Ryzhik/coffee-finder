import { StoresActionTypes, GetStoresAction } from "../constants/stores";

export function getStoresAction(): GetStoresAction {
  return {
    type: StoresActionTypes.GET_ALL_STORES,
  };
}
