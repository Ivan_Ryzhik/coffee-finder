export enum StoresActionTypes {
  GET_ALL_STORES = "GET_ALL_STORES",
  GET_ALL_STORES_REDUCER = "GET_ALL_STORES_REDUCER",
}

export interface GetStoresAction {
  type: StoresActionTypes.GET_ALL_STORES;
}
