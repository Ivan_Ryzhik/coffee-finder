import { put, all, takeLatest, call } from "redux-saga/effects";
import { StoresActionTypes } from "../constants/stores";
import { STORES_URL } from "../../constants/endpoints";

const fetchData = async () => {
  try {
    const response = await fetch(STORES_URL);
    const data = await response.text();
    return data;
  } catch (error) {
    console.log(error);
  }
};

function* getStoresSaga() {
  try {
    const res = yield call(fetchData);
    yield put({
      type: StoresActionTypes.GET_ALL_STORES_REDUCER,
      payload: res,
    });
  } catch (error) {
    // yield put(actions.signInRequestFailedAction());
  }
}

export default function* storesSaga() {
  yield all([
    yield takeLatest(StoresActionTypes.GET_ALL_STORES, getStoresSaga),
  ]);
}
