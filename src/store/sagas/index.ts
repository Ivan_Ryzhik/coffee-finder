import { all, fork } from "redux-saga/effects";
import storesSaga from "./stores";

export default function* rootSaga() {
  yield all([fork(storesSaga)]);
}
