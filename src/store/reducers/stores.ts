import { StoresActionTypes } from "../constants/stores";

export interface Stores {
  stores: string;
}

const initialState: Stores = {
  stores: "",
};

const storeReducer = (state: Stores = initialState, action): Stores => {
  const { type, payload } = action;
  switch (type) {
    case StoresActionTypes.GET_ALL_STORES_REDUCER:
      return {
        ...state,
        stores: payload,
      };
    default:
      return state;
  }
};

export default storeReducer;
