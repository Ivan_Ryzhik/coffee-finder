import { combineReducers, Reducer } from "redux";
import { connectRouter, RouterState } from "connected-react-router";
import { LocationState, History } from "history";
import auth, { Auth } from "./auth";
import stores, { Stores } from "./stores";

export interface RootReducer {
  auth: Auth;
  stores: Stores;
  router: RouterState;
}

export default (history: History<LocationState>): Reducer<RootReducer> =>
  combineReducers({
    auth,
    stores,
    router: connectRouter(history),
  });
