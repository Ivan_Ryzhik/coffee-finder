import React, { Component } from "react";
import { styled } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export const PADDING_X_UNIT = 3;
export const PADDING_Y_UNIT = 3;

const Wrapper = styled("main")(({ theme: { spacing } }: { theme: Theme }) => ({
  position: "relative",
  display: "flex",
  alignContent: "center",
  justifyContent: "center",
  width: "100%",
  height: `100vh`,
  overflow: "auto",
  padding: spacing(PADDING_X_UNIT),
}));

const Main = ({
  children,
  ...props
}: React.PropsWithChildren<Partial<Component>>) => {
  return <Wrapper {...props}>{children}</Wrapper>;
};

export default Main;
