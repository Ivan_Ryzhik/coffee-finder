import React, { useEffect, ReactElement } from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import { testAuthAction, signOutAction } from "store/actions";

import HomePage from "pages/HomePage";
import ProtectedPage from "pages/ProtectedPage/Loadable";
import SignInPage from "pages/SignInPage/Loadable";
import SignUpPage from "pages/SignUpPage/Loadable";
import NotFoundPage from "pages/NotFoundPage/Loadable";
import ProtectedRoute from "components/ProtectedRoute";
import Main from "components/Main";
import BaseStyles from "components/BaseStyles/BaseStyles";

import "./App.css";
import { RootReducer } from "./store/reducers";

interface AppProps {
  testAuth: () => void;
}

function App({ testAuth }: AppProps): ReactElement {
  useEffect(() => {
    testAuth();
  }, [testAuth]);

  return (
    <>
      <BaseStyles />
      <Main>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <ProtectedRoute path="/protected" component={ProtectedPage} />
          <Route path="/signup" component={SignUpPage} />
          <Route path="/signin" component={SignInPage} />
          <Route path="" component={NotFoundPage} />
        </Switch>
      </Main>
    </>
  );
}

export default connect(
  (store: RootReducer) => ({
    user: store.auth.user,
  }),
  { testAuth: testAuthAction, signOut: signOutAction }
)(App);
