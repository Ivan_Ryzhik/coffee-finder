import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import { useDispatch, useSelector } from "react-redux";

import CircularProgress from "@material-ui/core/CircularProgress";
import Notification from "../../components/Stores/notification";

import "./styles.css";

import { getStoresAction } from "../../store/actions/stores";
import getDistance from "../../utils/helpers";

export default function HomePage() {
  const dispatch = useDispatch();
  const storesString = useSelector((state: any) => state.stores.stores);
  const [position, setPosition] = useState({ latitude: 0, longitude: 0 });
  const [storesData, setStoresData] = useState([]);
  const [error, setError] = useState("");
  const [isLoading, setIsloading] = useState(false);
  const [open, setOpen] = React.useState(false);

  const onChange = ({ coords }) => {
    setPosition({
      latitude: coords.latitude,
      longitude: coords.longitude,
      // you can paste your own coordinates here below for testing
      // latitude: 56.068244,
      // longitude: 37.818165,
    });
  };

  const onError = (errorData: any) => {
    setError(errorData.message);
    setIsloading(false);
    setOpen(true);
  };

  const getUserLoacation = () => {
    // Here we will recieve user coordinates or bloking error
    const geo = navigator.geolocation;
    if (!geo) {
      setError("Geolocation is not supported");
      return;
    }
    geo.watchPosition(onChange, onError);
  };

  const getAllShops = () => {
    setIsloading(true);
    // action to get endpoint data
    dispatch(getStoresAction());
    getUserLoacation();
  };

  const getDistancetoStore = (storesObject) => {
    // add distance to each object
    const newData: any = Object.values(storesObject).map((item: any) => {
      const distance: number = getDistance(
        item.latitude,
        item.longitude,
        position.latitude,
        position.longitude
      );
      const newItem: any = item;
      newItem.distance = distance;
      return newItem;
    });
    // sort whole array by distance
    newData.sort((a, b) => a.distance - b.distance);
    setStoresData(newData);
    setIsloading(false);
  };

  useEffect(() => {
    if (storesString) {
      // build array of the objects from the string
      const storesLines = storesString.split("\n");
      const data = storesLines.map((store) => store.split(","));
      const storesObject: any = data.map((store) => {
        const item: {} = {
          location: store[0],
          latitude: store[1],
          longitude: store[2],
          distance: "",
        };
        return item;
      });
      setStoresData(storesObject);
      if (position.latitude !== 0 && position.longitude !== 0) {
        getDistancetoStore(storesObject);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [position]);

  const renderStoresList = () => {
    if (storesData[0] && !isLoading) {
      return (
        <ul>
          {storesData.map((store: any, index) => {
            return (
              <li key={store.distance} className={index < 3 ? "active" : ""}>
                <span>{store.location}</span> -{" "}
                <span>
                  {store.distance && store.distance.toFixed(4)} kilometers
                </span>
              </li>
            );
          })}
        </ul>
      );
    }
    return null;
  };

  return (
    <div className="container">
      <Notification error={error} open={open} setOpen={() => setOpen(true)} />
      {isLoading && (
        <div className="preloader">
          <CircularProgress />
        </div>
      )}
      <div className="story">
        You have been hired by a company that builds a mobile app for coffee
        addicts. You are responsible for taking the user’s location and
        returning a list of the three closest coffee shops.
      </div>
      <div className="story-button">
        <Button variant="contained" color="primary" onClick={getAllShops}>
          Find Stores
        </Button>
      </div>
      <div className="storesList">{renderStoresList()}</div>
    </div>
  );
}
