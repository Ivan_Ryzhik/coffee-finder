// Place here all endpoint addresses that app makes api calls to.
// for example:
// export const SIGN_UP_ENDPOINT = '/api/signup';
export const SIGN_IN_ENDPOINT = "";
export const SIGN_UP_ENDPOINT = "";
export const TEST_AUTH_ENDPOINT = "";
export const REFRESH_ACCESS_TOKEN_ENDPOINT = "";
export const STORES_URL =
  "https://raw.githubusercontent.com/allinfra-ltd/test_oop/master/coffee_shops.csv?token=AA5HVELWEJRKDGHELZ6232DAOHE54";
